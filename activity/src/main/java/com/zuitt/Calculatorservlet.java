package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class Calculatorservlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3593611354178072184L;
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws IOException {
	response.setContentType("text/html");
	PrintWriter out= response.getWriter();
	out.println("<html><head><title>Servlet CalculatorServlet</title></head><body>");
	int n1= Integer.parseInt(request.getParameter("num1"));
	int n2= Integer.parseInt(request.getParameter("num2"));
	int result=0;
	String opr= request.getParameter("opr");
	if(opr.equals("add"))
		result=n1+n2;
	System.out.println("The two you provided are:"+n1+","+n2);
	System.out.println("The operation that you wanted is:"+opr);
	System.out.println("The result is:"+result);
	
	if(opr.equals("subtract"))
		result=n1-n2;
	System.out.println("The two you provided are:"+n1+","+n2);
	System.out.println("The operation that you wanted is:"+opr);
	System.out.println("The result is:"+result);
	
	
	if(opr.equals("multiply"))
		result=n1*n2;
	System.out.println("The two you provided are:"+n1+","+n2);
	System.out.println("The operation that you wanted is:"+opr);
	System.out.println("The result is:"+result);
	
	if(opr.equals("divide"))
		result=n1/n2;
	System.out.println("The two you provided are:"+n1+","+n2);
	System.out.println("The operation that you wanted is:"+opr);
	System.out.println("The result is:"+result);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		
		int n1= Integer.parseInt(request.getParameter("num1"));
		int n2= Integer.parseInt(request.getParameter("num2"));
		int result=0;
		String opr= request.getParameter("opr");
		
		if(opr.equals("add"))
			result=n1+n2;
		System.out.println("The two you provided are:"+n1+","+n2);
		System.out.println("The operation that you wanted is:"+opr);
		System.out.println("The result is:"+result);
		
		if(opr.equals("subtract"))
			result=n1-n2;
		System.out.println("The two you provided are:"+n1+","+n2);
		System.out.println("The operation that you wanted is:"+opr);
		System.out.println("The result is:"+result);
		
		if(opr.equals("multiply"))
			result=n1*n2;
		System.out.println("The two you provided are:"+n1+","+n2);
		System.out.println("The operation that you wanted is:"+opr);
		System.out.println("The result is:"+result);
		
		if(opr.equals("divide"))
			result=n1/n2;
		System.out.println("The two you provided are:"+n1+","+n2);
		System.out.println("The operation that you wanted is:"+opr);
		System.out.println("The result is:"+result);
		
		PrintWriter writer= response.getWriter();
	
		String htmlRespone = "<html>";
        htmlRespone += "The two you provided are:" + n1 + "," +n2 +"<br/>";      
        htmlRespone += "The operation that you wanted is" + opr+ "<br/>";
        htmlRespone += "The result is" + result;
        htmlRespone += "</html>";
         
        // return response
        writer.println(htmlRespone);
		
	}



}
