package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 
43283931101411089L;
	
	public void doPost(HttpServletRequest req, 
HttpServletResponse res)throws IOException {
		System.out.println("Hello from the calculator servlet.");
		/*
		 *  The parameter names are defined in the form input
		 *  field.
		 *  The parameter are found in the url as query string
		 *  (e.g. ?num1=&num2)
		 */
		int num1 =Integer.parseInt(req.getParameter("num1"));
		int num2 =Integer.parseInt(req.getParameter("num2"));
	
		int total = num1 + num2;
		// the getwriter(method is used to print out information in the browser as a response.
		PrintWriter out= res.getWriter();
		
		out.println("<h1>The total of the two numbers are"+ total+"</h1>");
	}
	
}
